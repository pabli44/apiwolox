package com.pvelilla.wolox2.apiwolox2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Apiwolox2Application {

	public static void main(String[] args) {
		SpringApplication.run(Apiwolox2Application.class, args);
	}

}
