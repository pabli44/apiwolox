package com.pvelilla.wolox2.apiwolox2.service;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;

import com.pvelilla.wolox2.apiwolox2.model.Albumes;

/**
 * @author pvelillag
 *
 */
public interface AlbumesService {

	public ArrayList<Albumes> listar() throws Exception;
	
	public ArrayList<Albumes> listarxUsuario(String usuario) throws Exception;
	
	public ArrayList listarxId(String id) throws Exception;
	
	public ResponseEntity<String> registrarAlbumCompartido(String idAlbumAcompartir, String title, String usuario, String permisos) throws Exception;
	
	public ResponseEntity<String> cambiarPermisosAlbumxUsuario(String idAlbum, String usuario, String permisos) throws Exception;
}
