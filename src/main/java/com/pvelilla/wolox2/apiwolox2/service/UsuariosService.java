package com.pvelilla.wolox2.apiwolox2.service;

import java.util.ArrayList;

import com.pvelilla.wolox2.apiwolox2.model.Usuarios;

/**
 * @author pvelillag
 *
 */
public interface UsuariosService {
	
	public ArrayList<Usuarios> listar() throws Exception;
	
	public ArrayList buscarComentariosxname(String name) throws Exception;
	
	public ArrayList buscarUsuariosxPermisoxAlbum(String idAlbum, String permiso) throws Exception;
}
