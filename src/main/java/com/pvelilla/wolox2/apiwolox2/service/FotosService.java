package com.pvelilla.wolox2.apiwolox2.service;

import java.util.ArrayList;

import com.pvelilla.wolox2.apiwolox2.model.Fotos;

/**
 * @author pvelillag
 *
 */
public interface FotosService {

	public ArrayList<Fotos> listar() throws Exception;
	public ArrayList listarxUsuario(String usuario) throws Exception;
		
}
