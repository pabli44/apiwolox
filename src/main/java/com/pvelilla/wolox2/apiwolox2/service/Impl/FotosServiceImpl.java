package com.pvelilla.wolox2.apiwolox2.service.Impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.pvelilla.wolox2.apiwolox2.model.Fotos;
import com.pvelilla.wolox2.apiwolox2.service.AlbumesService;
import com.pvelilla.wolox2.apiwolox2.service.FotosService;
import com.pvelilla.wolox2.apiwolox2.utilities.UtilApi;

/**
 * @author pvelillag
 *
 */

@Service
public class FotosServiceImpl implements FotosService, UtilApi{
	
	private static String json;
	private static String dir = "photos";
	
	@Autowired
	private AlbumesService albumesService;
	
	@Override
	public ArrayList<Fotos> listar() throws Exception{
		ArrayList<Fotos> listaFotos = new ArrayList();
		try {
			json = returnJsonFromUrl(json, dir);
			Gson gson = new Gson();
			listaFotos = gson.fromJson(json, ArrayList.class);
			
		}catch(Exception e){}
		
		return listaFotos;
	}
	
	@Override
	public ArrayList listarxUsuario(String usuario) throws Exception{
		ArrayList listaFotosxUsuario = new ArrayList();
		ArrayList listaAlbumesxUsuario = new ArrayList();
		ArrayList listaFotos = new ArrayList();
		
		try {
			//albumes por usuario
			listaAlbumesxUsuario = albumesService.listarxUsuario(usuario);
			
			LinkedTreeMap<Object,Object> t;
			LinkedTreeMap<Object,Object> tFotos;
			String idAlbumAlbum = "";
			String idAlbumFotos = "";
			String idAlbumArr = "";
			for(int i=0;i<listaAlbumesxUsuario.size();i++) {
	        	t = (LinkedTreeMap) listaAlbumesxUsuario.get(i);
	        	 
	        	idAlbumAlbum = t.get("id").toString();
	        	idAlbumAlbum = idAlbumAlbum.substring(0,idAlbumAlbum.indexOf("."));
	        	
	        	if(i==listaAlbumesxUsuario.size()-1) {
	        		idAlbumArr += idAlbumAlbum;
	        	}else {
	        		idAlbumArr += idAlbumAlbum+",";
	        	}
	        	
	        }
			
			//fotos x usuario
			listaFotos = listar();
			String idAlbumArrSplit[] = idAlbumArr.split(",");
			for(int i=0;i<listaFotos.size();i++) {
				//Buscar fotos por id de album
	        	tFotos = (LinkedTreeMap) listaFotos.get(i);
	        	idAlbumFotos = tFotos.get("albumId").toString();
	        	idAlbumFotos = idAlbumFotos.substring(0,idAlbumFotos.indexOf("."));
	        	
	        	for(int j=0;j<idAlbumArrSplit.length;j++){
	        		if(idAlbumFotos.equals(idAlbumArrSplit[j])) {
	        			listaFotosxUsuario.add(tFotos);
	        			break;
	        		}
	        	}
	        	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return listaFotosxUsuario;
	}
	
}
