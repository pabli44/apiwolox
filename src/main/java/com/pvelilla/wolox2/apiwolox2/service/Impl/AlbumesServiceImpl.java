package com.pvelilla.wolox2.apiwolox2.service.Impl;

import java.util.ArrayList;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;
import com.pvelilla.wolox2.apiwolox2.model.Albumes;
import com.pvelilla.wolox2.apiwolox2.service.AlbumesService;
import com.pvelilla.wolox2.apiwolox2.utilities.UtilApi;

/**
 * @author pvelillag
 *
 */

@Service
public class AlbumesServiceImpl implements AlbumesService, UtilApi {
	
	private static String json;
	private static String dir = "albums";
	
	@Override
	public ArrayList<Albumes> listar() throws Exception{
		ArrayList<Albumes> listaAlbumes = new ArrayList();
		try {
			json = returnJsonFromUrl(json, dir);
			Gson gson = new Gson();
			listaAlbumes = gson.fromJson(json, ArrayList.class);
			
		}catch(Exception e){}
		
		return listaAlbumes;
	}
	
	@Override
	public ArrayList listarxUsuario(String usuario) throws Exception{
		ArrayList listaAlbumes = new ArrayList();
		ArrayList listaAlbumesxUsuario = new ArrayList();
		
		try {
			json = returnJsonFromUrl(json, dir);
			Gson gson = new Gson();
			listaAlbumes = gson.fromJson(json, ArrayList.class);
			
			LinkedTreeMap<Object,Object> t;
			String usuarioLista = "";
	        for(int i=0;i<listaAlbumes.size();i++) {
	        	t = (LinkedTreeMap) listaAlbumes.get(i);
	        	 
	        	usuarioLista = t.get("userId").toString();
	        	usuarioLista = usuarioLista.substring(0,usuarioLista.indexOf("."));
	        	
	        	if(usuario.equalsIgnoreCase(usuarioLista)) {
	        		listaAlbumesxUsuario.add(t);
	        	}
	        }
			
		}catch(Exception e){}
		
		return listaAlbumesxUsuario;
	}
	
	@Override
	public ArrayList listarxId(String id) throws Exception{
		ArrayList listaAlbumes = new ArrayList();
		ArrayList listaAlbumesxUsuario = new ArrayList();
		
		try {
			json = returnJsonFromUrl(json, dir);
			Gson gson = new Gson();
			listaAlbumes = gson.fromJson(json, ArrayList.class);
			
			LinkedTreeMap<Object,Object> t;
			String idLista = "";
	        for(int i=0;i<listaAlbumes.size();i++) {
	        	t = (LinkedTreeMap) listaAlbumes.get(i);
	        	 
	        	idLista = t.get("id").toString();
	        	idLista = idLista.substring(0,idLista.indexOf("."));
	        	
	        	if(id.equalsIgnoreCase(idLista)) {
	        		listaAlbumesxUsuario.add(t);
	        	}
	        }
			
		}catch(Exception e){}
		
		return listaAlbumesxUsuario;
	}
	
	@Override
	public ResponseEntity<String> registrarAlbumCompartido(String idAlbumAcompartir, String title, String usuario, 
			String permisos) throws Exception{
		
		String uri = dataUrl+"albums";
		ResponseEntity<String> response = null;
		
		try {
			String register = "[{\"user\":\""+usuario+"\",\"can\":\""+permisos+"\"}]";
			JsonParser parser = new JsonParser();
			JsonElement jsonE = parser.parse(register);
			RestTemplate restTemplate = new RestTemplate();
		    JsonObject jo = new JsonObject();
		    jo.addProperty("idAlbum", idAlbumAcompartir);
		    jo.addProperty("title", title);
		    jo.add("shareUserCan", jsonE);

		    HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    headers.set("Authorization", "Basic " + "xxxxxxxxxxxx");
		    HttpEntity<String> entity = new HttpEntity<String>(jo.toString(), headers);

		    response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return response;
	
	}
	
	@Override
	public ResponseEntity<String> cambiarPermisosAlbumxUsuario(String idAlbum, String usuario, String permisos) throws Exception{
		ResponseEntity<String> response = null;
		ArrayList listaAlbumes = new ArrayList();
		LinkedTreeMap<Object,Object> t;
		JsonArray ja = null;
		JsonObject jo = null;
		
		try {
			listaAlbumes = listarxId(idAlbum);
			t = (LinkedTreeMap) listaAlbumes.get(0);
			ja = new JsonArray();
			ja = (JsonArray)t.get("shareUserCan");
			jo = new JsonObject();
			
			for(int i=0;i<ja.size();i++) {
				jo = ja.get(i).getAsJsonObject();
				
				if(usuario.equals(jo.get("user").toString())) {
					jo.addProperty("can", permisos);
					break;
				}
			}
			
		}catch (Exception e) {
			e.getLocalizedMessage();
		}
		
		
		
		return response;
	}
	
	
}
