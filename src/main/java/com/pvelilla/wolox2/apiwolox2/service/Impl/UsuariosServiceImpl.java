package com.pvelilla.wolox2.apiwolox2.service.Impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.pvelilla.wolox2.apiwolox2.model.Usuarios;
import com.pvelilla.wolox2.apiwolox2.service.AlbumesService;
import com.pvelilla.wolox2.apiwolox2.service.UsuariosService;
import com.pvelilla.wolox2.apiwolox2.utilities.UtilApi;

/**
 * @author pvelillag
 *
 */

@Service
public class UsuariosServiceImpl implements UsuariosService, UtilApi{
	
	private static String json;
	
	@Autowired
	private AlbumesService albumesService;
	
	@Override
	public ArrayList<Usuarios> listar() throws Exception{
		ArrayList<Usuarios> listaUsuarios = new ArrayList<Usuarios>();
		try {
			String dir = "users";
			json = returnJsonFromUrl(json, dir);
			Gson gson = new Gson();
			listaUsuarios = gson.fromJson(json, ArrayList.class);
			
		}catch(Exception e){}
		
		return listaUsuarios;
	}
	
	@Override
	public ArrayList buscarComentariosxname(String name) throws Exception {
		ArrayList listaComentarios = new ArrayList();
		ArrayList listaComentariosxname = new ArrayList();
		
		try {
			String dir = "comments";
			json = returnJsonFromUrl(json, dir);
			Gson gson = new Gson();
			listaComentarios = gson.fromJson(json, ArrayList.class);
			
			String nameListaComentarios = "";
			LinkedTreeMap<Object,Object> t;
			for(int i=0;i<listaComentarios.size();i++) {
				t = (LinkedTreeMap) listaComentarios.get(i);
				nameListaComentarios = t.get("name").toString();
				
				if(name.equals(nameListaComentarios)) {
					listaComentariosxname.add(t);
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return listaComentariosxname;
	}
	
	@Override
	public ArrayList buscarUsuariosxPermisoxAlbum(String idAlbum, String permiso) throws Exception{
		ArrayList listaUsuariosxPermisoxAlbum = new ArrayList();
		ArrayList listaAlbunesxId = new ArrayList();
		LinkedTreeMap<Object,Object> t;
		JsonArray ja = null;
		JsonObject jo = null;
		String permisosCadena = "";
		String permisosCadenaSplit[] = null; 
		
		try {
			listaAlbunesxId = albumesService.listarxId(idAlbum);
			t = (LinkedTreeMap) listaAlbunesxId.get(0);
			ja = new JsonArray();
			ja = (JsonArray)t.get("shareUserCan");
			
			for(int i=0;i<ja.size();i++) {
				jo = ja.get(i).getAsJsonObject();
				permisosCadena = jo.get("can").toString(); 
				permisosCadenaSplit = permisosCadena.split(",");
				
				for(int j=0;j<permisosCadenaSplit.length;j++) {
					if(permiso.equals(permisosCadenaSplit[j])) {
						//se agrega a la lista de usuarios
						listaUsuariosxPermisoxAlbum.add(jo.get("user").toString());
					}
				}
			}
			
		}catch (Exception e) {
			e.getLocalizedMessage();
		}
		
		return listaUsuariosxPermisoxAlbum;
	}
}
