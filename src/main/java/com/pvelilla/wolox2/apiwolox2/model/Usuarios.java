package com.pvelilla.wolox2.apiwolox2.model;

import java.io.Serializable;

/**
 * @author pvelillag
 *
 */
public class Usuarios implements Serializable{
	private String name;
	private String userName;
	private String email;
	private String[] adress;
	private String phone;
	private String website;
	private String[] company;
	
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the adress
	 */
	public String[] getAdress() {
		return adress;
	}
	/**
	 * @param adress the adress to set
	 */
	public void setAdress(String[] adress) {
		this.adress = adress;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}
	/**
	 * @param website the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}
	/**
	 * @return the company
	 */
	public String[] getCompany() {
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(String[] company) {
		this.company = company;
	}
}
