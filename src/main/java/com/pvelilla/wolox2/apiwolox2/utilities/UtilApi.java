package com.pvelilla.wolox2.apiwolox2.utilities;

import java.net.URL;
import java.util.Scanner;

/**
 * @author pvelillag
 *
 */
public interface UtilApi {
	final static String dataUrl = "http://jsonplaceholder.typicode.com/";
	
	public default String returnJsonFromUrl(String json, String dir) throws Exception{
		return new Scanner(new URL(dataUrl+dir).openStream(), "UTF-8").useDelimiter("\\A").next();
	}
}
