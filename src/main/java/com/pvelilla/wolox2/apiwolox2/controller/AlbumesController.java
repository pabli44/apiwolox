package com.pvelilla.wolox2.apiwolox2.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pvelilla.wolox2.apiwolox2.model.Albumes;
import com.pvelilla.wolox2.apiwolox2.service.AlbumesService;

/**
 * @author pvelillag
 *
 */

@RestController
@RequestMapping(path = "/albumes")
public class AlbumesController {
private ArrayList listaAlbumes;
	
	@Autowired
	private AlbumesService albumesService;
	
	@GetMapping("/listar")
	public ArrayList listar(){
		listaAlbumes = new ArrayList();
		
		try {
			listaAlbumes = albumesService.listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listaAlbumes;
	}
	
	@GetMapping("/listarxusuario")
	public ArrayList<Albumes> listarxUsuario(@RequestParam String usuario){
		ArrayList<Albumes> listaAlbumesxUsuario = new ArrayList();
		
		try {
			listaAlbumesxUsuario = albumesService.listarxUsuario(usuario);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listaAlbumesxUsuario;
	}
	
	@GetMapping("/listarxid")
	public ArrayList<Albumes> listarxId(@RequestParam String id){
		ArrayList<Albumes> listaAlbumesxId = new ArrayList();
		
		try {
			listaAlbumesxId = albumesService.listarxId(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listaAlbumesxId;
	}
	
	@PostMapping("/registrarlbumcompartido")
	public ResponseEntity<String> registrarAlbumCompartido(@RequestParam String idalbumacompartir, 
			@RequestParam String title, @RequestParam String usuario, @RequestParam String permisos) {
		
		ResponseEntity<String> response = null;
		try {
			response = albumesService.registrarAlbumCompartido(idalbumacompartir, title, usuario, permisos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	@PutMapping("/cambiarpermisosalbumxusuario")
	public ResponseEntity<String> cambiarPermisosAlbumxUsuario(@RequestParam String idalbum,  @RequestParam String usuario, 
			@RequestParam String permisos){
		
		ResponseEntity<String> response = null;
		try {
			response = albumesService.cambiarPermisosAlbumxUsuario(idalbum, usuario, permisos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
		
	}
	
	
}
