package com.pvelilla.wolox2.apiwolox2.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pvelilla.wolox2.apiwolox2.service.FotosService;

/**
 * @author pvelillag
 *
 */

@RestController
@RequestMapping(path = "/fotos")
public class FotosController {
	private ArrayList listaFotos;
	
	@Autowired
	private FotosService fotosService;
	
	@GetMapping("/listar")
	public ArrayList listar(){
		listaFotos = new ArrayList();
		
		try {
			listaFotos = fotosService.listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listaFotos;
	}
	
	@GetMapping("/listarxusuario")
	public ArrayList listarFotosxUsuario(@RequestParam String usuario) {
		ArrayList listaFotosxUsuario = new ArrayList();
		
		try {
			listaFotosxUsuario = fotosService.listarxUsuario(usuario);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listaFotosxUsuario;
	}
	
}
