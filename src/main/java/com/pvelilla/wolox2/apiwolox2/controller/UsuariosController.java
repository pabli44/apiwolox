package com.pvelilla.wolox2.apiwolox2.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pvelilla.wolox2.apiwolox2.model.Usuarios;
import com.pvelilla.wolox2.apiwolox2.service.UsuariosService;

/**
 * @author pvelillag
 *
 */

@RestController
@RequestMapping(path = "/usuarios")
public class UsuariosController {
	
	private ArrayList<Usuarios> listaUsuarios;
	
	@Autowired
	private UsuariosService usuariosService;
	
	@GetMapping("/listar")
	public ArrayList<Usuarios> listar(){
		listaUsuarios = new ArrayList<Usuarios>();
		
		try {
			listaUsuarios = usuariosService.listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listaUsuarios;
	}
	
	@GetMapping("/buscarComentariosxname")
	public ArrayList buscarComentariosxname(@RequestParam String name) {
		ArrayList listaComentariosxname = new ArrayList();
		try {
			listaComentariosxname = usuariosService.buscarComentariosxname(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listaComentariosxname;
	}
	
	@GetMapping("/buscarusuariosxpermisoxalbum")
	public ArrayList buscarUsuariosxPermisoxAlbum(@RequestParam String idalbum, @RequestParam String permiso) {
		ArrayList listaUsuariosxPermisoxAlbum = new ArrayList();
		try {
			listaUsuariosxPermisoxAlbum = usuariosService.buscarUsuariosxPermisoxAlbum(idalbum, permiso);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listaUsuariosxPermisoxAlbum;
	}
	
}
