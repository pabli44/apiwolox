# api

Api para Wolox: usuarios, albumes, fotos, comentarios.


Parte 1:
<br>
Funciones API:
<br>

1. Obtener usuarios:<br>
http://localhost:8080/usuarios/listar
<br><br>

2. Las fotos:<br>
http://localhost:8080/fotos/listar
<br><br>

3. Los álbumes del sistema y de cada usuario:<br> 
http://localhost:8080/albumes/listar
<br><br>
http://localhost:8080/albumes/listarxusuario?usuario=2
<br><br>

4. Plus: ​ Las fotos de un usuario:<br>
http://localhost:8080/fotos/listarxusuario?usuario=2
<br><br>

Parte 2:
<br>

1- Registrar album compartido con usuario, y permisos:<br>

convenciones: 
<br>
permisos[1--> lectura, 2--> escritura]
<br>
http://localhost:8080/albumes/registrarlbumcompartido?idalbumacompartir=10&title=hibryd heory&usuario=4&permiso=1,2
<br><br>

2- Cambiar los permisos de un usuario para un álbum determinado:<br> 
http://localhost:8080/albumes/cambiarpermisosalbumxusuario?idalbum=10&usuario=4&permisos=2,1
<br><br>

3- Traer   todos   los   usuarios   que   tienen   un   permiso   determinado   respecto   a   un  
álbum específico<br> 
http://localhost:8080/usuarios/buscarusuariosxpermisoxalbum?idalbum=3&permiso=2
<br><br>

Otro punto:
Comentarios fitrados por el parámetro "name":<br>
http://localhost:8080/usuarios/buscarComentariosxname?name=dolores minus aut libero

